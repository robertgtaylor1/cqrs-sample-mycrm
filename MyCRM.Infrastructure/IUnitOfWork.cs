﻿using System;
using System.Linq;
using MyCRM.Infrastructure.Storage;

namespace MyCRM.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        IQueryable<T> Query<T>() where T : class, new();
        void Delete<T>(T entity) where T: class, IEntity, new();
    	void Delete(object entity);
    	void Store<T>(T entity) where T: class, IEntity, new();
        void Store(object entity);
        void SaveChanges();
    }
}