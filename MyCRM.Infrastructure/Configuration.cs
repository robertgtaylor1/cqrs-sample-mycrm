﻿using System.Configuration;

namespace MyCRM.Infrastructure
{
	public class Configuration
	{
		public static string ReadModelConnectionString
		{
			get { return ConfigurationManager.AppSettings["ReadModelConnectionString"]; }
		}

        public static string ReadModelDbName
        {
            get { return ConfigurationManager.AppSettings["ReadModelDbName"]; }
        }

		public static string EventStoreConnectionString
		{
			get { return ConfigurationManager.AppSettings["EventStoreConnectionString"]; }
		}

		public static string EventStoreDbName
		{
			get { return ConfigurationManager.AppSettings["EventStoreDbName"]; }
		}
	}
}
