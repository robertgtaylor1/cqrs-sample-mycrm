﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using StructureMap;
using StructureMap.ServiceLocatorAdapter;

namespace MyCRM.Infrastructure {

	public class ServiceLocator
	{
		private static IServiceLocator _serviceLocator;

		[Obsolete("Wird derzeit benötigt um zur Design-Zeit Zugriffe auf den ServiceLocator zu verhindern - das muss aber anders gelöst werden")]
		public static bool IsInitialized
		{
			get { return _serviceLocator != null; }
		}

		public static void RegisterContainer(IContainer container)
		{
			_serviceLocator = new StructureMapServiceLocator(container);
		}

		public static object GetService(Type serviceType)
		{
			return _serviceLocator.GetService(serviceType);
		}

		public static object GetInstance(Type serviceType)
		{
			return _serviceLocator.GetInstance(serviceType);
		}

		public static object GetInstance(Type serviceType, string key)
		{
			return _serviceLocator.GetInstance(serviceType, key);
		}

		public static IEnumerable<object> GetAllInstances(Type serviceType)
		{
			return _serviceLocator.GetAllInstances(serviceType);
		}

		public static TService GetInstance<TService>()
		{
			return _serviceLocator.GetInstance<TService>();
		}

		public static TService GetInstance<TService>(string key)
		{
			return _serviceLocator.GetInstance<TService>(key);
		}

		public static IEnumerable<TService> GetAllInstances<TService>()
		{
			return _serviceLocator.GetAllInstances<TService>();
		}
	}
}
