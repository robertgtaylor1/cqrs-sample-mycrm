﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyCRM.Infrastructure.Storage
{
    public class MongoDBUnitOfWork : IUnitOfWork
    {
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Query<T>() where T : class, new()
        {
            throw new NotImplementedException();
        }

        public void Delete<T>(T entity) where T : class, IEntity, new()
        {
            throw new NotImplementedException();
        }

        public void Delete(object entity)
        {
            throw new NotImplementedException();
        }

        public void Store<T>(T entity) where T : class, IEntity, new()
        {
            throw new NotImplementedException();
        }

        public void Store(object entity)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }
    }
}
