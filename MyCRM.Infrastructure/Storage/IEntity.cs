﻿using System;

namespace MyCRM.Infrastructure.Storage
{
	public interface IEntity
	{
		Guid Id { get; set; }
	}
}