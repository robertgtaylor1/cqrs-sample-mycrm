﻿using System;

namespace MyCRM.WebApplication.Models
{
    public class CreateNewCustomerViewModel
    {
        public Guid CustomerId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}