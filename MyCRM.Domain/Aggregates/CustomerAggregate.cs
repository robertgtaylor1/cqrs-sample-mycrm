﻿using System;
using MyCRM.Domain.Entities;
using MyCRM.Domain.Messages.Events;
using MyCRM.Infrastructure;

namespace MyCRM.Domain.Aggregates
{
    class CustomerAggregate : AggregateRoot
    {
        Customer _customer;

        public void CreateNewCustomer(Guid customerId, string lastName, string firstName)
        {
            ApplyChangeBy(new NewCustomerCreatedEvent()
                {
                    AggregateId = customerId,
                    CustomerId = customerId,
                    LastName = lastName,
                    FirstName = firstName
                });
        }

        void Apply(NewCustomerCreatedEvent domainEvent)
        {
            AggregateId = domainEvent.CustomerId;
            _customer = new Customer()
                {
                    LastName = domainEvent.LastName,
                    FirstName = domainEvent.FirstName
                };
        }

        public void CreateNewCustomerAddress(string street, string number, string city, string postalCode)
        {
            ApplyChangeBy(new NewCustomerAddressCreatedEvent()
                {
                    CustomerId = AggregateId,
                    Street = street,
                    Number = number,
                    City = city,
                    PostalCode = postalCode
                });
        }

        void Apply(NewCustomerAddressCreatedEvent domainEvent)
        {
            _customer.City = domainEvent.City;
            _customer.Number = domainEvent.Number;
            _customer.PostalCode = domainEvent.PostalCode;
            _customer.Street = domainEvent.Street;
        }

        public void ChangeCustomerAddress(string street, string number, string city, string postalCode)
        {
            ApplyChangeBy(new CustomerAddressChangedEvent()
                {
                    CustomerId = AggregateId,
                    Street = street,
                    Number = number,
                    City = city,
                    PostalCode = postalCode
                });
        }

        void Apply(CustomerAddressChangedEvent domainEvent)
        {
            _customer.Street = domainEvent.Street;
            _customer.Number = domainEvent.Number;
            _customer.City = domainEvent.City;
            _customer.PostalCode = domainEvent.PostalCode;
        }
    }
}
