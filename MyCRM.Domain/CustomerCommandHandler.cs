﻿using System;
using MyCRM.Domain.Aggregates;
using MyCRM.Domain.Messages.Commands;
using MyCRM.Infrastructure.Messaging;

namespace MyCRM.Domain
{
    public class CustomerCommandHandler : 
        ICommandHandler<CreateNewCustomerCommand>,
        ICommandHandler<CreateNewCustomerAddressCommand>,
        ICommandHandler<ChangeCustomerAddressCommand>
    {
        public void Execute(CreateNewCustomerCommand command)
        {
            var customerAggregate = new CustomerAggregate();

            customerAggregate.CreateNewCustomer(command.CustomerId, command.LastName, command.FirstName);

            this.SaveAggregate(customerAggregate);

            // TODO: Insert ExceptionHandling like RejectCommand

        }

        public void Execute(CreateNewCustomerAddressCommand command)
        {
            var customerAggregate = this.GetAggregateBy<CustomerAggregate>(command.CuatomerId);

            customerAggregate.CreateNewCustomerAddress(command.Street, command.Number, command.City, command.PostalCode);

            this.SaveAggregate(customerAggregate);

            // TODO: Insert ExceptionHandling like RejectCommand

        }

        public void Execute(ChangeCustomerAddressCommand command)
        {
            var customerAggregate = this.GetAggregateBy<CustomerAggregate>(command.CuatomerId);

            customerAggregate.ChangeCustomerAddress(command.Street, command.Number, command.City, command.PostalCode);

            this.SaveAggregate(customerAggregate);

            // TODO: Insert ExceptionHandling like RejectCommand

        }
    }
}
