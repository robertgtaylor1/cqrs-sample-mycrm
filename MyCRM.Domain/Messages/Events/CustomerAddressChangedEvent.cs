﻿using System;
using MyCRM.Infrastructure.Messaging;

namespace MyCRM.Domain.Messages.Events
{
    public class CustomerAddressChangedEvent : DomainEvent
    {
        public Guid CustomerId { get; set; }

        public string Street { get; set; }

        public string Number { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }
    }
}